Pig Latin Translator
=================

A simple application to translate text from ordinary English to Pig Latin.

Requirements
------------

Web Project for Nette 3.0 requires PHP 7.1 (developed to version PHP 7.4)
