<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\model\Translator\PigLatinTranslatorModel;
use Nette\Application\Responses\JsonResponse;

final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var PigLatinTranslatorModel @inject * */
    public $pigLatinTranslator;

    /**
     * Render default template variables
     */
    public function renderDefault()
    {
        $this->template->title = 'Homepage';
    }

    /**
     * Action to translate the word to Pig Latin
     *
     * @param string $input Text to translate
     * @return string Pig Latin text
     * @throws Nette\Application\AbortException
     */
    public function actionTranslate(string $input): string
    {
        $translation = $this->pigLatinTranslator->getPigTranslate($input);
        $this->sendResponse(new JsonResponse($translation));
    }
}
