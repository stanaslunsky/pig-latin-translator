<?php

namespace App\model\Translator;

use App\PigLatinTranslatorConstants;

class PigLatinTranslatorModel
{
    public $pigLatinTranslatorRules;

    /**
     * PigLatinTranslatorModel constructor.
     */
    public function __construct()
    {
        $this->pigLatinTranslatorRules = new PigLatinTranslatorRules();
    }

    /**
     * @param string $userInput
     * @return string
     */
    public function getPigTranslate(string $userInput): string
    {
        if (empty($userInput))
            return '';

        $userInput = strtolower($userInput);

        $words = explode(' ', $userInput);
        $pigTranslate = "";

        foreach ($words as $word) {
            $word = $this->translateWord($word);

            // In case of first symbol is not in english alphabet, do not add anything.
            if (!empty($word)) {
                $pigTranslate .= ' ' . $word;
            }
        }

        return ltrim($pigTranslate);
    }

    /**
     * Specify the following word processing method.
     *
     * @param string $word Word
     * @return string Pig Latin Translation
     */
    private function translateWord(string $word): string
    {
        $specialChar = '';

        if ($this->pigLatinTranslatorRules->checkForSpecialCharacters($word)) {
            $specialChar = substr($word, -1);
            $word = substr_replace($word, '', -1);
        }

        if ($this->pigLatinTranslatorRules->checkVowelsFirst($word)) {
            return $word . 'yay' . $specialChar;

        } elseif ($this->pigLatinTranslatorRules->checkConsonantsFirst($word)) {
            /**
             * Některé online překladače překládají slova bez samohlásek (např. 'str' pro ukázku) na 'rstay', podle zadání
             * jsem tomu spíš rozumněl jako 'stray', viz:
             *      In words that begin with consonant sounds, the initial consonant
             *      or consonant cluster is moved to the end of the word, and "ay" is added.
             */
            return preg_replace('/^(y*[' . PigLatinTranslatorConstants::CONSONANTS . ']*)([' . PigLatinTranslatorConstants::VOWELS . 'y].*)$/', "$2$1ay", $word) . $specialChar;
        }

        return '';
    }
}