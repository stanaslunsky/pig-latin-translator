<?php


namespace App;


class PigLatinTranslatorConstants
{
    const VOWELS = 'aeiou';
    const CONSONANTS = 'bcdfghjklmnqrstvwxz';
    const SPECIAL_CHARS = ',.!?';
}