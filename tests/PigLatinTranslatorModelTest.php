<?php

use Tester\Assert;
use App\model\Translator\PigLatinTranslatorModel;

require 'bootstrap.php';

/**
 * Test expected output.
 */

/** @var PigLatinTranslatorModel @inject * */
$pigLatinTranslator = new PigLatinTranslatorModel();

test('correct translation', function () use ($pigLatinTranslator) {
    $expectedTranslation = 'ellohay, iyay amyay anleystay, andyay ityay isyay eallyray oldcay outsideyay.';
    $textToTranslate = 'Hello, I am Stanley, and it is really cold outside.';

    Assert::same($expectedTranslation, $pigLatinTranslator->getPigTranslate($textToTranslate));
    Assert::same('ellowyay ylestay', $pigLatinTranslator->getPigTranslate('yellow style'));
    // Assert::same('ellowyay estylay', $translator->getPigTranslate('yellow style'));
});

test('empty input', function () use ($pigLatinTranslator) {
    $expectedTranslation = '';
    $textToTranslate = '';

    Assert::same($expectedTranslation, $pigLatinTranslator->getPigTranslate($textToTranslate));
});

test('only special characters', function () use ($pigLatinTranslator) {
    $expectedTranslation = '';
    $textToTranslate = '.!?';

    Assert::same($expectedTranslation, $pigLatinTranslator->getPigTranslate($textToTranslate));
});

// TODO validate all input chars
//test('national characters', function () use ($pigLatinTranslator) {
//    $expectedTranslation = 'eskycay umisyay?';
//    $textToTranslate = 'česky umíš?';
//
//    Assert::same($expectedTranslation, $pigLatinTranslator->getPigTranslate($textToTranslate));
//});
